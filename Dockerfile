FROM arm64v8/alpine

RUN wget https://github.com/gohugoio/hugo/releases/download/v0.81.0/hugo_0.81.0_Linux-ARM64.tar.gz && \
    tar -zxvf hugo_0.81.0_Linux-ARM64.tar.gz && \
    chmod +x hugo && \
    mv hugo /usr/bin/hugo 
